﻿Vue.component('menu',  Vue.extend({
  showDetails : (menuItem) ->
    #    modalInstance = $modal.open
    #      templateUrl: 'ImageDetail.html',
    #      controller: 'ModalInstanceCtrl',
    #      resolve:
    #        item: () ->
    #          menuItem
    return
  template: '#menu'
  menu :
    [
      title : 'Appetizers'
      items :[
        title : 'Cold Cuts Platter'
        img : 'images/ColdCut_3339.jpg'
        description : 'Cold cuts of smoked beef, Bosnian sausage, and Feta cheese. $8.'
      ,
        title : 'Feta Cheese'
        img : 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCI+PHJlY3Qgd2lkdGg9IjE0MCIgaGVpZ2h0PSIxNDAiIGZpbGw9IiNlZWUiPjwvcmVjdD48dGV4dCB0ZXh0LWFuY2hvcj0ibWlkZGxlIiB4PSI3MCIgeT0iNzAiIHN0eWxlPSJmaWxsOiNhYWE7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LXNpemU6MTJweDtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtkb21pbmFudC1iYXNlbGluZTpjZW50cmFsIj4xNDB4MTQwPC90ZXh0Pjwvc3ZnPg=='
        description : 'White brine cheese with a smooth, solid texture and a salty taste. $8.'
      ,
        title : 'Grilled Mushrooms'
        img : 'images/Mushrooms_3382.jpg'
        description : 'Grilled fresh mushrooms filled with Kajmak (Bosnian homemade cream cheese) and smoked beef. $8.'
      ]
    ,
      title : 'Soups and Stews'
      items :[
        title : 'Goulash'
        img : 'images/Goulash_3436.jpg'
        description : 'Thick, buttery broth with tender chunks of beef. Served with mashed potatoes. $8.'
      ,
        title : 'Vegetable Soup'
        img : 'images/Soup_3429.jpg'
        description : 'This flavorful, hearty soup is made with broccoli cauliflower, peas, potatoes and carrots. $4.'
      ]
    ,
      title : 'Salads'
      items :[
        title : 'Chicken Salad'
        img : 'images/ChickenCaesarSalad_3560.jpg'
        description : 'Salad with Garlic Croutons and grated Parmesan cheese topped with juicy, grilled sliced chicken breast. $7.5.'
      ,
        title : 'Shopska Salad'
        img : 'images/SopskaSalata_3404.jpg'
        description : 'Tomatoes, cucumber, pepper, on- ion, olives and feta cheese. $6.'
      ,
        title : 'Cabbage Salad'
        img : 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCI+PHJlY3Qgd2lkdGg9IjE0MCIgaGVpZ2h0PSIxNDAiIGZpbGw9IiNlZWUiPjwvcmVjdD48dGV4dCB0ZXh0LWFuY2hvcj0ibWlkZGxlIiB4PSI3MCIgeT0iNzAiIHN0eWxlPSJmaWxsOiNhYWE7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LXNpemU6MTJweDtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtkb21pbmFudC1iYXNlbGluZTpjZW50cmFsIj4xNDB4MTQwPC90ZXh0Pjwvc3ZnPg=='
        description : 'Fresh cabbage with oil and vinaigrette dressing. $3.5.'
      ]
    ,
      title : 'Quick Bite'
      items :[
        title : 'Calamari'
        img : 'images/Calamari_3816.jpg'
        description : 'Crispy deep fried calamari rings served with tomato sauce. $9.'
      ,
        title : 'Chicken Wings'
        img : 'images/Chicken-wings.jpg'
        description : 'Spicy or mild chicken wings served with ranch dressing. $6.00'
      ,
        title : 'Chicken Nuggets'
        img : 'images/Chicken-nuggets.jpg'
        description : 'Crispy chicken bites with your favorite barbecue hot sauce. $6.00.'
      ,
        title : 'Chicken Sandwich'
        img : 'images/chickenSandwich.jpg'
        description : 'Tender, juicy chicken strips grilled to perfection, served with crisp lettuce, juicy tomato and mayonnaise on a toasted bun. $6.00.'
      ]
    ,
      title : 'Pizza'
      subtitle : 'Additional Toppings: Beef Sausage, Mushrooms, Green Peppers, Tomatoes, Onions, Jalapeno, Black Olives, Mozzarella Cheese or Parmesan Cheese.   Sm. $1 Lg. $1.25 XL. $1.5'
      items :[
        title : 'Saray Pizza'
        img : 'images/Pizza_3828.jpg'
        description : 'Special homemade double cheese pizza with, smoked beef, beef sausage, mushrooms, tomato paste, Mozzarella cheese and black olives.   Sm. $11 Lg. $19 XL. $25.'
      ,
        title : 'Vegetarian Pizza'
        img : 'images/Pizza_3828.jpg'
        description : 'Onions, green peppers, mushrooms, black olives, tomatoes and cheese.  Sm. $10 Lg. $17 XL. $20.'
      ,
        title : 'Cheese Pizza'
        img : 'images/Pizza_3828.jpg'
        description : 'Tomato marinara sauce and Italian herb seasonings topped with Mozzarella cheese.  Sm. $8 Lg. $13 XL. $17.'
      ]
    ,
      title : 'Grilled Specialties'
      subtitle : 'Served with special homemade Bosnian flat pita bread known as “Lepina” baked on a pizza stone.'
      items :[
        title : 'Cevapi'
        img : 'images/Cevapi_3322.jpg'
        description : 'Famous Bosnian juicy specialty made from ground beef resembling small sausages. Served with raw chopped onion tucked inside pita bread. $7.  <span class="text-muted"> Add Kajmak (homemade cream cheese). $1</span>'
      ,
        title : 'Chicken or Beef Kebab'
        img : 'images/ChickenKabob00356.jpg'
        description : 'Grilled sliced chicken or sliced ribeye kebab served with your choice of rice, grilled vegetables or French fries. Chicken $9 Ribeye $11'
      ,
        title : 'Mixed Grill Platter'
        img : 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCI+PHJlY3Qgd2lkdGg9IjE0MCIgaGVpZ2h0PSIxNDAiIGZpbGw9IiNlZWUiPjwvcmVjdD48dGV4dCB0ZXh0LWFuY2hvcj0ibWlkZGxlIiB4PSI3MCIgeT0iNzAiIHN0eWxlPSJmaWxsOiNhYWE7Zm9udC13ZWlnaHQ6Ym9sZDtmb250LXNpemU6MTJweDtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtkb21pbmFudC1iYXNlbGluZTpjZW50cmFsIj4xNDB4MTQwPC90ZXh0Pjwvc3ZnPg=='
        description : 'A sampling of Bosnian dishes. Assorted grilled meats served with fresh cabbage salad. $13.'
      ,
        title : 'Grilled Tilapia'
        img : 'images/TilapiaFishFilet_8944.jpg'
        description : 'Served with lemon, onion, parsley, and your choice of rice, French fries or grilled vegetables. $11.'
      ,
        title : 'Saray Hamburger'
        img : 'images/Pljeskavica_3637.jpg'
        description : 'Large beef patty served on pita bread with Kajmak (homemade cream cheese) or Ajvar (roasted red pepper spread). $9.'
      ,
        title : 'Angus Hamburger'
        img : 'images/Hamburger_3626.jpg'
        description : 'Homemade Angus hamburger with cheese and garnish on our special bun. $5 <span class="text-muted">Add French fries $2.</span>'
      ,
        title : 'Gyros Plate'
        img : 'images/Gyros_3673.jpg'
        description : 'Served with lemon, onion, parsley, and your choice of rice, French fries or grilled vegetables. $11.'
      ]
    ,
      title : 'Bosnian Pitas'
      subtitle : '“Pita” is famous pastries made of a thin flaky dough known as phyllo.  It can be filled with meat, cheese, spinach or other vegetables. Our pita is homemade and baked to perfection on a pizza stone.'
      items :[
        title : 'Cheese Pita'
        img : 'images/Sirnica_3364.jpg'
        description : 'Pita filled with mix of 3-cheese blend and eggs. $6 '
      ,
        title : 'Burek'
        img : 'images/Burek_3480.jpg'
        description : 'Most famous pita filled with lean ground beef, onion and spices. $6.'
      ,
        title : 'Spinach Pita'
        img : 'images/Zeljanica_3937.jpg'
        description : 'Healthy pita made from fresh spinach, eggs and 3-cheese blend. $6.'
      ]
    ,
      title : 'Deserts'
      items :[
        title : 'Apple Rolls'
        img : 'images/Jabukovaca_3740.jpg'
        description : 'Bosnian style apple pie, similar to strudels $2'
      ,
        title : 'Shampita'
        img : 'images/Sampita_3731.jpg'
        description : '$2.'
      ,
        title : 'Creampita'
        img : 'images/Krempita_3717.jpg'
        description : '$2.5'
      ,
        title : 'Hurmasica'
        img : 'images/Hormasice.png'
        description : 'Bosnian biscuit $2'
      ,
        title : 'Baklava'
        img : 'images/baklava.jpg'
        description : 'Famous Greek walnut filled desert $2.5'
      ,
        title : 'Crepes'
        img : 'images/crepes.jpg'
        description : 'French crepes with Nutella or jam, walnut and ice cream $5.'
      ,
        title : 'Cakes'
        img : ''
        description : 'Over 50 types of cakes available to order.'
      ]
    ]
}))

#angular.module('saray-grill')
#.controller 'ModalInstanceCtrl', ($scope, $modalInstance, item) ->
#    $scope.item = item
#    $scope.ok = () ->
#        $modalInstance.close()
