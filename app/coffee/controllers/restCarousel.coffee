﻿Vue.component('restCarousel', {
  myInterval : 4000
  slides : [
    image: 'images/SarayGrillOutsideSunny.jpg'
    title: 'Exterior'
    text: ''
  ,
    image: 'images/SarayGrill-Interior.jpg'
    title: 'Interior'
    text: ''
  ,
    image: 'images/SarayGrill-Patio.jpg'
    title: 'Patio'
    text: ''
  ,
    image: 'images/SarayGrill-Patio2.jpg'
    title: 'Patio'
    text: ''
  ]
})
