﻿app = new Vue(
  el: '#app',
  data:
    currentView: 'home'
)

router = new Router(
  '/':        ()-> app.currentView = 'home'
  '/:view': (view) ->
    app.currentView = view
    #app.$.view.subview = view
)
router.init()