#dependencies
gulp      = require 'gulp'
coffee    = require 'gulp-coffee'
less      = require 'gulp-less'
minifyCSS = require 'gulp-minify-css'
concat    = require 'gulp-concat'
uglify    = require 'gulp-uglify'
imagemin  = require 'gulp-imagemin'
rimraf    = require 'rimraf'
refresh   = require 'gulp-livereload'
lr        = require 'tiny-lr'
server    = lr();


paths =
  coffee: ['app/coffee/controllers/*.coffee', 'app/coffee/*.coffee']
  images: 'client/img/**/*'


EXPRESS_PORT = 4000
EXPRESS_ROOT = 'app'

# Let's make things more readable by
# encapsulating each part's setup
# in its own method
startExpress = ->
  express = require("express")
  app = express()
  app.use express.static(EXPRESS_ROOT)
  app.listen EXPRESS_PORT
  return

# Not all tasks need to use streams
# A gulpfile is just another node program and you can use all packages available on npm
gulp.task 'clean', (cb)->
  rimraf 'app/js/**', cb


gulp.task 'scripts', ['clean'], () ->
  #Minify and copy all JavaScript (except vendor scripts)
  gulp.src(paths.coffee)
    .pipe(coffee {bare: true})
    .pipe(uglify())
    .pipe(concat 'app.min.js')
    .pipe(gulp.dest 'app/js')
    .pipe(refresh server)

gulp.task('styles', ->
  gulp.src(['app/less/img-slider.less', 'app/less/main.less'])
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest 'app/styles')
    .pipe(refresh server )
)

gulp.task('lr-server', () ->
  server.listen(35729, (err) ->
    if(err)
      return console.log err
  )
)

gulp.task 'default', () ->
  startExpress()

  gulp.run 'lr-server', 'scripts', 'styles'

  gulp.watch('app/coffee/**', (event) ->
    gulp.run 'scripts'
  )

  gulp.watch( 'app/less/**', (event) ->
    gulp.run 'styles'
  )
